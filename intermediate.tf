resource "tls_private_key" "intermediate" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_cert_request" "intermediate" {
  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.intermediate.private_key_pem

  subject {
    common_name         = lookup(var.intermediate_subject, "common_name")
    organization        = coalesce(lookup(var.intermediate_subject, "organization", ""), lookup(var.root_subject, "organization", ""))
    organizational_unit = coalesce(lookup(var.intermediate_subject, "organizational_unit", ""), lookup(var.root_subject, "organizational_unit", ""))
    locality            = coalesce(lookup(var.intermediate_subject, "locality", ""), lookup(var.root_subject, "locality", ""))
    province            = coalesce(lookup(var.intermediate_subject, "province", ""), lookup(var.root_subject, "province", ""))
    country             = coalesce(lookup(var.intermediate_subject, "country", ""), lookup(var.root_subject, "country", ""))
  }
}

resource "tls_locally_signed_cert" "intermediate" {
  cert_request_pem      = tls_cert_request.intermediate.cert_request_pem
  ca_private_key_pem    = tls_private_key.root.private_key_pem
  ca_cert_pem           = tls_self_signed_cert.root.cert_pem
  ca_key_algorithm      = "RSA"
  validity_period_hours = var.intermediate_ca_validity_period_hours
  is_ca_certificate     = true
  allowed_uses          = tls_self_signed_cert.root.allowed_uses
}
