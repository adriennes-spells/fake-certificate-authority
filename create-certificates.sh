#!/bin/bash

set -e

mkdir -p ${AUTHORITY_DATA_PATH}

terraform init
terraform get -update
terraform apply

TERRAFORM_OUTPUT="$(terraform output -json)"

echo ${TERRAFORM_OUTPUT} | jq -r '.root_ca_certificate_pem.value'         > ${AUTHORITY_DATA_PATH}/root_ca_certificate.pem
echo ${TERRAFORM_OUTPUT} | jq -r '.intermediate_ca_private_key_pem.value' > ${AUTHORITY_DATA_PATH}/intermediate_ca_private_key.pem
echo ${TERRAFORM_OUTPUT} | jq -r '.intermediate_ca_certificate_pem.value' > ${AUTHORITY_DATA_PATH}/intermediate_ca_certificate.pem
