resource "tls_private_key" "client" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_cert_request" "client" {
  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.client.private_key_pem

  subject {
    common_name = "Client"
  }

  ip_addresses = [jsondecode(data.http.ip.body).ip]
}

resource "tls_locally_signed_cert" "client" {
  cert_request_pem      = tls_cert_request.client.cert_request_pem
  ca_private_key_pem    = tls_private_key.intermediate.private_key_pem
  ca_cert_pem           = tls_locally_signed_cert.intermediate.cert_pem
  ca_key_algorithm      = "RSA"
  validity_period_hours = var.intermediate_ca_validity_period_hours

  allowed_uses = [
    "key_encipherment",
    "key_agreement",
    "digital_signature",
    "client_auth",
  ]
}
