output "root_ca_certificate_pem" {
  value = tls_self_signed_cert.root.cert_pem
}

output "intermediate_ca_private_key_pem" {
  value     = tls_private_key.intermediate.private_key_pem
  sensitive = true
}

output "intermediate_ca_certificate_pem" {
  value = tls_locally_signed_cert.intermediate.cert_pem
}

output "client_certificate" {
  value = tls_locally_signed_cert.client.cert_pem
}

output "client_ca_chain" {
  value = join("", [
    tls_locally_signed_cert.client.cert_pem,
    tls_locally_signed_cert.intermediate.cert_pem,
    tls_self_signed_cert.root.cert_pem
  ])
}

output "client_private_key" {
  value     = tls_private_key.client.private_key_pem
  sensitive = true
}
